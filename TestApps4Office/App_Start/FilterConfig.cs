﻿using System.Web;
using System.Web.Mvc;

namespace TestApps4Office
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
